from sense_emu import SenseHat
import time

sense = SenseHat()

j = (251, 255, 0) # jaune
n = (0, 0, 0) # noir
b = (255, 255, 255) # blanc
r = (161, 39, 66) # bordeaux
c = (236, 123, 203) # rose

direction = "up"

sense.clear()
time.sleep(2)

sense.show_message(':aw', text_colour=[251, 255, 0])

while True:
	
	#==========================#
	# GET SENSE HAT DATA	   #
	#==========================#

	# Get joystick events
	for event in sense.stick.get_events():
		direction = event.direction
	
	# Get orientation
	_, _, pitch = sense.get_orientation().values()
	tongue = pitch >= 180
	
	# Get the temperature
	t = sense.get_temperature_from_pressure() 
	
	#==========================#
	# DRAW					   #
	#==========================#
	
	blueQty = 240 - int(t * 0.5)
	j = (251, blueQty, 0)
	
	# Start from an empty face
	face = [
		j,j,j,j,j,j,j,j,
		j,b,b,j,j,b,b,j,
		j,b,b,j,j,b,b,j,
		j,j,j,j,j,j,j,j,
		j,r,r,r,r,r,r,j,
		j,j,r,r,r,r,j,j,
		j,j,j,r,r,j,j,j,
		j,j,j,j,j,j,j,j,
	]
	
	# Eyes
	if direction == 'up':
		face[10] = n;
		face[14] = n;
	elif direction == 'left':
		face[9] = n;
		face[13] = n;
	elif direction == 'down':
		face[17] = n;
		face[21] = n;
	elif direction == 'right':
		face[18] = n;
		face[22] = n;
		
	# Tongue
	if tongue:
		face[51] = c;
	else:
		face[52] = c;
	
	sense.set_pixels(face)

